import React, {useState} from 'react';
import './App.css';
import FirstTask from "../firstTask/firstTask";
import SecondTask from "../secondTask/secondTask";

function App() {
  const [componentName, setComponentName] = useState();

  let component;

  switch (componentName) {
    case 'first':
      component = <FirstTask/>;
      break;
    case 'second':
      component = <SecondTask/>;
      break;
    default:
      component = null
  }

  return (
      <>
        <div className="switcher">
          <button
              type="button"
              onClick={() => setComponentName('first')}>First Task</button>
          <button
              type="button"
              onClick={() => setComponentName('second')}>Second Task</button>
        </div>
        {component}
      </>
  );
};

export default App;
