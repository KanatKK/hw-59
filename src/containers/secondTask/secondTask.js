import React, {useEffect, useState} from 'react';
import Joke from "../../components/joke/joke";

const SecondTask = () => {
    const [joke, setJoke] = useState([]);

        useEffect(() => {
            const fetchData = async () => {
                const response = await fetch('https://api.chucknorris.io/jokes/random');
                if (response.ok) {
                    const joke = await response.json();
                    setJoke(joke)
                };
            };
            fetchData()
        }, []);

    return (
        <div className='container'>
            <Joke joke={joke.value}/>
        </div>
    );
};

export default SecondTask;