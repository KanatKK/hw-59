import React, {Component} from 'react';
import './firstTask.css'
import Movies from "../../components/movies/movies";

class FirstTask extends Component {
    state = {
        names:[{movie: ''}],
        movies: []
    };

    render() {
        let addMovie = event => {
            let namesCopy = this.state.names;
            namesCopy.movie = event.target.value;
            this.setState(namesCopy);
        };

        let getMovie = () => {
                this.state.movies.push({name: this.state.names.movie});
                this.setState(this.state);
        };

        let twoWayBinding = (index, event) => {
            let moviesCopy = [...this.state.movies];
            moviesCopy[index].name = event.target.value;
            this.setState(moviesCopy);
        };

        let deleteMovie = index => {
            this.state.movies.splice(index, 1);
            this.setState(this.state);
        };

        return (
            <div className="container">
                <input type="text" className="movieName" onChange={addMovie}/>
                <button type="button" className="addMovie" onClick={getMovie}>Add</button>
                <h4>To watch list:</h4>
                {this.state.movies.map((movie, index) => (
                    <Movies
                        key={index} name={movie.name}
                        twoWayBinding={event => twoWayBinding(index, event)}
                        delete={() => deleteMovie(index)}
                    />
                ))}
            </div>
        );
    };
};

export default FirstTask;