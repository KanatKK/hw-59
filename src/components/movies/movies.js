import React, {Component} from 'react';
import './movies.css'

class Movies extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return(this.props.name !== nextProps.name);
    };

    render() {

        return (
            <>
                <input
                    type="text" className="movie"
                    value={this.props.name} onChange={this.props.twoWayBinding}
                />
                <button className="deleteMovie" onClick={this.props.delete}>X</button>
            </>
        );
    };
};

export default Movies;